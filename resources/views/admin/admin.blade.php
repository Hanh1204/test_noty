<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="uid" content="{{optional(auth()->user())->id}}">
  <meta name="device-token" content="{{optional(auth()->user())->device_token}}">
  <title>
    Manas | Admin
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
    name='viewport' />
  <!--     Fonts and icons     -->
  <base href={{asset('')}}>
  <link rel="stylesheet" type="text/css"
    href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link href="dists/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="dists/css/font-awesome.min.css">
  <link rel="stylesheet" href="dists/plugin/tag_input/bootstrap-tagsinput.css">
</head>

<body class="">
  <div class="wrapper ">
    <div class="main-panel">
      <!-- Navbar -->
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          @yield('content-admin')
        </div>
        <div class="toast">
          <div class="toast-body alert alert-primary"></div>
        </div>
      </div>
    </div>
  </div>



  <!--   Core JS Files   -->
  <script src="dists/js/core/jquery.min.js"></script>
  <script src="dists/js/core/popper.min.js"></script>
  <script src="dists/js/bootstrap.min.js"></script>
  <script src="dists/js/plugins/dropzone.js"></script>
  <script src="dists/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="dists/js/plugins/moment.min.js"></script>
  <!-- Chartist JS -->
  <script src="dists/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="dists/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="dists/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>
  <script src="{{ asset('dists/plugin/ckeditor/ckeditor.js') }}"></script>
  <!-- Firebase cloud message init -->
  
  <!-- Firebase cloud message init -->
  <script src="https://www.gstatic.com/firebasejs/7.10.0/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/7.10.0/firebase-messaging.js"></script>
  <script src="https://www.gstatic.com/firebasejs/7.10.0/firebase-analytics.js"></script>
  <script src="firebase.js"></script>
  <!-- Firebase cloud message init -->
  <script src="dists/js/select2.min.js"></script>
  <script src="dists/js/custom.js"></script>
  <script src="dists/js/admin.js"></script>
  

</body>

</html>
