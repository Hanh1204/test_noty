<?php

namespace App\Traits;

use App\Models\Notification;
use App\User;
use FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

trait NotificationTrait
{

    public function sendNotifcationEvent($title, $event, $current_user)
    {
        $tokens = User::userNotify($current_user)->pluck('device_token')->toArray();
        $action = route('admin.event.edit', $event->id);

        if (empty($tokens)) {
            return true;
        }

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 60);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($title)->setSound('default')->setClickAction($action);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['event_id' => $event->id]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
    }
    // setClickAction

    // ADMIN SEND NOTIFICATION EVENT
    public function notifcationDoctorComment($title, $body, $sender, $receiver)
    {
        $tokens = $receiver->device_token;
        $action = 'NOTIFICATION_COMMENT';
        $notify = $this->createNotifcation($title, $body, $sender, $receiver);
        if (empty($tokens)) { return true; }
        $html = view('admin.blocks.notify', compact('notify'))->render();

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 60);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)->setSound('default')->setClickAction($action);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['inner_html' => $html]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
    }

    protected function createNotifcation($title, $body, $sender, $receiver)
    {
        return Notification::create([
            'user_id' => $sender->id,
            'receiver_id' => $receiver->id,
            'image' => $sender->avatar,
            'title' => $title,
            'body' => $body
        ]);
    }
}
