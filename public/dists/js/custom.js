Dropzone.autoDiscover = false;
$(document).ready(function () {
  $('.alert').fadeOut(2500);

  $('#add_service').on('click', function(e){
    e.preventDefault();
    $('form #inp-redirect_service').val(1);
    $('form').trigger('submit');
  });

  $('.sidebar li.dropdown').each(function () {
    if ($(this).hasClass('active')) {
      $(this).find('.nav-dropdown').removeClass('collapsed');
      $(this).find('.dropdown_menu').addClass('show');
    }
  });

  $('.btn-delete').on('click', function () {
    let status = confirm('Bạn có chắc chắn xóa không?');
    return status;
  });

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
  // DROPZONE FOR CREATE FORM
  if ($('#image-upload').length > 0) {
    let url = $('#image-upload').attr('url');
    let code = $('#image-upload').attr('code');
    let url_remove = $('#attachment_id_input').attr('url-remove');
    let $add_btn = $('.dropzone').find(".add-image");
    var dropzone = new Dropzone("#image-upload", {
      paramName: "file",
      maxFilesize: 100,
      timeout: 100000,
      addRemoveLinks: true,
      maxFiles: 20,
      url: url,
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      },
      clickable: '.add-image, .dropzone',
      init: function () {
        let dzObj = this;
        let name = $('#attachment_id_input').attr('name');
        let init_image = $('#image-upload').attr('data-images');

        init_image = init_image ? JSON.parse(init_image) : [];
        $.each(init_image, function (index, item) {
          let data = item;
          let mockFile = { name: data.file_name, size: data.file_size, type: 'image/png', dataURL: data.file_path };
          let input = "<input type='hidden' name='" + name + "' value='" + JSON.stringify(item) + "' data-id='" + data.id + "'/>";
          dzObj.options.addedfile.call(dzObj, mockFile);
          dzObj.options.thumbnail.call(dzObj, mockFile, data.file_path);
          dzObj.options.success.call(dzObj, mockFile);
          dzObj.options.complete.call(dzObj, mockFile);
          dzObj.files.push(mockFile);
          $(mockFile.previewElement).append(input);
        });

        dzObj.files.length > 0 ? $add_btn.removeClass("hidden") : $add_btn.addClass("hidden");
      }
    }).on("addedfile", function(file) {
      dropzone.files.length > 0
      ? $add_btn.removeClass('hidden')
      : $add_btn.addClass('hidden')
      if(dropzone.files.length > dropzone.options.maxFiles) {
        alert("No more files please!");
        this.removeFile(file);
      }
    }).on('sending', function (file, xhr, formData) {
      formData.append('code', code);
    }).on("success", function (file, response) {
      let data = JSON.stringify(response.data);
      let name = $('#attachment_id_input').attr('name');
      let input = "<input type='hidden' name='" + name + "' value='" + data + "'/>";
      $(file.previewTemplate).append(input);
    }).on("error", function (file, errorMessage, xhr) {
      $(file.previewTemplate).find('.dz-error-message span').text(errorMessage.errors.detail.file);
    }).on("removedfile", function (file, errorMessage, xhr) {
      if(dropzone.files.length == 0){
        $add_btn.addClass('hidden');
      }
      let id = $(file.previewTemplate).find('input').attr('data-id');
      if (id != null && id != 'undefined') {
        removeImage(url_remove, id);
      }
    });
  }

});


function removeImage(url, id) {
  $.ajax({
    type: "POST",
    url: url,
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    data: { attchment_id: id },
    error: function (xhr, status, error) {
      console.error('AJAX Error: ' + status + error);
    },
    success: function (response) {
      var notify = $.notify(response.message, {
        type: 'success',
        allow_dismiss: false,
        showProgressbar: false,
        animate: {
          enter: 'animated fadeInRight',
          exit: 'animated fadeOutRight'
        }
      });
      setTimeout(function () {
        notify.close();
      }, 1500);
    }
  });
}


// INIT MAP
$(document).ready(function () {
  var script = document.createElement('script');
  script.onload = function () { };
  script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBXF9PpYh__77Bse8b-Vb7cBEjW8x34r-E&callback=initialize";
  if ($('#map-form').length > 0) {
    document.body.appendChild(script);
  }
});
if ($('#map-form').length > 0) {
  var geocoder;
  var map;
  var marker;

  function initialize() {
    geocoder = new google.maps.Geocoder();
    var uluru = { lat: 16.066708, lng: 108.213120 };
    map = new google.maps.Map(document.getElementById('map-form'), {
      zoom: 12,
      center: uluru
    });
    initMarker(map);

    google.maps.event.addListener(map, 'click', function (event) {
      let latLng = { lat: event.latLng.lat(), lng: event.latLng.lng() };
      setInputLocation(latLng);
      setMarker(map, latLng);
    });
  }

  function setInputLocation(location) {
    $('#inp-lat').attr('value', location.lat);
    $('#inp-lng').attr('value', location.lng);
  }

  function setMarker(map, latLng) {
    map.setCenter(latLng);
    if (marker) {
      marker.setPosition(latLng);
    } else {
      marker = new google.maps.Marker({
        position: latLng,
        map: map
      });
    }
  }

  function initMarker(map) {
    let lat = $('#inp-lat').attr('value');
    let lng = $('#inp-lng').attr('value');
    if (lat != null && lng != null) {
      let latLng = { lat: parseFloat(lat), lng: parseFloat(lng) };
      map.setCenter(latLng);
      marker = new google.maps.Marker({
        position: latLng,
        map: map
      });
    }
  }

  $(document).ready(function () {
    initialize();
  });
}

$(document).on('change', '#inp-baby_category_id', function () {
  let id = $(this).val();
  $('#inp-sub_category_id').empty();
  $.ajax({
    type: "GET",
    url: 'api/v1/baby/sub-categories/' + id,
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    error: function (xhr, status, error) {
      console.error('AJAX Error: ' + status + error);
    },
    success: function (response) {
      let defalt_option = '<option value="">Chọn danh mục con</option>'
      $('#inp-sub_category_id').append(defalt_option);

      $.each(response.data, function (index, value) {
        let option = '<option value="' + value.id + '">' + value.name + '</option>'
        $('#inp-sub_category_id').append(option);
      });
    }
  });
});


$(document).ready(function () {
  $(document).on('change keyup', '.max_guest', function () {
    let val = parseInt($(this).val());
    if (!isNaN(val)) {
      if (val < 0) {
        $(this).val(0).change();
      } else if (val > 9999) {
        $(this).val(9999).change();
      }
    }
  });

  $(document).on('change keyup','.max_guest', function () {
    let $container = $(this).closest('.item-time');
    var sum = 0;
    $container.find('.max_guest').each(function () {
      let val = parseInt($(this).val());
      if (!isNaN(val)) {
        sum = sum + val;
      }
    });
    $container.find('.sum-quantity').text(sum);
  });

  $('.table-schdule').find('.item-time').each(function () {
    let sum = 0
    $(this).find('.max_guest').each(function () {
      let val = parseInt($(this).val());
      if (!isNaN(val)) {
        sum = sum + val;
      }
    });
    if (sum != 0) {
      $(this).find('.sum-quantity').text(sum);
    }
  });
  // AJAX GENERATE TABLE
  const generateUrl = $('#generate_table_url').data('url');
  $('.end-time').on('dp.hide', function (e) {
    e.preventDefault();
    let $container = $(this).closest('.item-time');
    let end_time = $(this).val();
    let start_time = $container.find('.start-time').val();
    let code = $(this).attr('code');
    generate_table(code, start_time, end_time, $container);
  })

  function generate_table(code, start, end, container){
    $.ajax({
      type: "POST",
      url: generateUrl,
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
        'Cache-Control': 'no-cache, no-store'
      },
      cache: false,
      data:{
        code: code,
        start_time: start,
        end_time: end
      },
      error: function (xhr, status, error) {
        let ajax_error = JSON.parse(xhr.responseText);
        let message = Object.values(ajax_error.errors.detail)[0];
        container.find('.show-table').html('<div class="invalid-feedback show">'+message+'</div>');
      },
      success: function (response) {
        container.find('.show-table').html(response.html);
      }
    });
  }
});
$(document).ready(function() {
  $.each($(".js-example-basic-multiple"), function(){
    let value = JSON.parse($(this).attr('value'))
    $(this).select2({
      language: "en",
    });
    $(this).val(value).select2();
  });

});